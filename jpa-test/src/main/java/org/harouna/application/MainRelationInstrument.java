package org.harouna.application;

import java.util.Calendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.eclipse.persistence.internal.libraries.asm.commons.InstructionAdapter;
import org.harouna.model.Instrument;
import org.harouna.model.InstrumentType;
import org.harouna.model.MusicType;
import org.harouna.model.Musician;

public class MainRelationInstrument {

	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		Calendar calendar = Calendar.getInstance();
		// TODO Auto-generated method stub
		
		Musician musician1 = new Musician();
		
		
		//musician1.setId(1L);  //Id g�n�r� automatiquement dans la classe Musician
		musician1.setName("keith Richard");
		musician1.setMusicType(MusicType.ROCK);
		calendar.set(2000, 8,16);
		musician1.setDateOfBirth(calendar.getTime());
		
		Instrument instrument1 = new Instrument();
		instrument1.setName("Guitar");
		
		instrument1.setInstrumentType(InstrumentType.CORD);
		
		musician1.setInstrument(instrument1);
		instrument1.setMusician(musician1);
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-test");

		EntityManager em = emf.createEntityManager();
		
		//Create
		em.getTransaction().begin();
		
		em.persist(musician1);
		em.persist(instrument1);

		em.getTransaction().commit();
		
		System.out.println(musician1);
		System.out.println(instrument1);
	}

}
