package org.harouna.application;

import java.util.Calendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.harouna.model.Instrument;
import org.harouna.model.InstrumentType;
import org.harouna.model.MusicType;
import org.harouna.model.Musician;
import org.harouna.model.Song;

public class MainRelationSong {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Calendar calendar = Calendar.getInstance();
		
		Musician musician = new Musician();
		
		
		//musician1.setId(1L);  //Id g�n�r� automatiquement dans la classe Musician
		musician.setName("keith Richard");
		musician.setMusicType(MusicType.ROCK);
		calendar.set(2000, 8,16);
		musician.setDateOfBirth(calendar.getTime());
		
		Instrument instrument1 = new Instrument();
		instrument1.setName("Guitar");		
		instrument1.setInstrumentType(InstrumentType.CORD);
		
		musician.setInstrument(instrument1);
		instrument1.setMusician(musician);
		
		//Song
		Song song1 = new Song();
		song1.setName("If i were a boy");
		song1.setMusician(musician);
		
		Song song2 = new Song();
		song2.setName("To my unborn child");
		song2.setMusician(musician);

		Song song3 = new Song();
		song3.setName("Until the end of time");
		song3.setMusician(musician);
		
		musician.getSongs().add(song1);
		musician.getSongs().add(song2);
		musician.getSongs().add(song3);
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-test");
		EntityManager em = emf.createEntityManager();
		
		//Create
		em.getTransaction().begin();
		
		em.persist(musician);

		em.getTransaction().commit();
		
		System.out.println(musician);
		System.out.println(instrument1);
		System.out.println(musician.getSongs());
	}

}
