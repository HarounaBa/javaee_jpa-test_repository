package org.harouna.application;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.harouna.model.Adress;
import org.harouna.model.MusicType;
import org.harouna.model.Musician;

public class MainEntity {

	public static void main(String[] args) {

		Calendar calendar = Calendar.getInstance();
		// TODO Auto-generated method stub
		
		Musician musician1 = new Musician();
		
		
		//musician1.setId(1L);  //Id g�n�r� automatiquement dans la classe Musician
		musician1.setName("Youssou Ndour");
		musician1.setMusicType(MusicType.MBALAKH);
		calendar.set(2000, 8,16);
		musician1.setDateOfBirth(calendar.getTime());
		
		Adress adress1 = new Adress();
		adress1.setAdress("17 av JB Clement 93430 Villetaneuse");
		musician1.setAdress(adress1);
		
		Musician musician2 = new Musician();
		//musician2.setId(2L); 
		musician2.setName("2pac");
		musician2.setMusicType(MusicType.ROCK);
		calendar.set(1997, 8,16);
		musician2.setDateOfBirth(calendar.getTime());

		Musician musician3 = new Musician();
		//musician3.setId(3L);
		musician3.setName("Maalox");
		musician3.setMusicType(MusicType.SLAM);
		calendar.set(1990, 8,16);
		musician3.setDateOfBirth(calendar.getTime());

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-test");

		EntityManager em = emf.createEntityManager();
		
		//Generation code SQL, on prend les objets + �criture dans la base *******************
		//On ouvre une transaction avant chaque cr�ation , 
		//modification ou suppression de la table
		//Ensuite on ferme la transaction
		
		em.getTransaction().begin();
		
		em.persist(musician1);
		em.persist(musician2);
		em.persist(musician3);

		em.getTransaction().commit();

		//Retrieve*******************************************************
		Musician musician = em.find(Musician.class, 3L); //On part chercher la classe ayant pour id=3
														 // et on le r�cup�re
		
		
		/*	System.out.println("Musician 1 : " + musician1.getId());
		System.out.println("Musician 2 : " + musician2.getId());
		System.out.println("Musician 3 : " + musician3.getId()); 
		 */

		System.out.println("Musician pour PK 3 : " + musician);

		//Update**********************************************************
		em.getTransaction().begin();

		musician = em.find(Musician.class, 1L);
		calendar.set(1959, 9, 1);
		musician.setDateOfBirth(calendar.getTime());

		em.getTransaction().commit();

		//Delete*********************************************************
		em.getTransaction().begin();

		musician = em.find(Musician.class, 2L);
		em.remove(musician);

		em.getTransaction().commit();	

	}

}
