package org.harouna.application;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.harouna.model.Musician;

public class MainRelationReadInstrument {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-test");

		EntityManager em = emf.createEntityManager();
		
		Musician musician = em.find(Musician.class, 1L);
		
		System.out.println(musician);
		System.out.println(musician.getInstrument());
		System.out.println(musician.getInstrument().getMusician());
		
	}
}
